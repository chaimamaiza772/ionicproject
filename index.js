
const my_btn1 = document.querySelector("#mybtn1");
const my_btn2 = document.querySelector("#mybtn2");
const TotSalaries = document.querySelector("#TotSalaries");
const TotEmployees = document.querySelector("#TotEmployees");
const controller = document.querySelector("ion-alert-controller");
const list = document.querySelector("#List");
const countries = [
  { id: "0", name: "Khaoula", age: "29", salaire: "1600" },
  { id: "1", name: "Omar", age: "27", salaire: "1000" },
  { id: "2", name: "Chaima", age: "24", salaire: "1000" },
  { id: "3", name: "Oussema", age: "13", salaire: "300" }
];

// Display list
function FetchAll() {
  //Total Employees
  document.getElementById("TotEmployees").innerHTML = countries.length;
  
  //Total salairies
    var TotSalaries = 0;
    for (i = 0; i < countries.length; i++) {
      TotSalaries = Number(countries[i].salaire) + TotSalaries;
    }
    document.getElementById("TotSalaries").innerHTML = TotSalaries;
 


  var data = "";
  if (countries.length > 0) {
    for (i = 0; i < countries.length; i++) {
      data += "<ion-item>";
      data += "<ion-label>" + countries[i].name + "</ion-label> ";
      data +=
        '<ion-icon name="create" slot="end" color="primary"  onclick="tUpdate(' +
        i +
        ')"></ion-icon> ';
      data +=
        '<ion-icon name="trash" slot="end" color="danger" onclick="tDelete(' +
        i +
        ') "></ion-icon> </ion-item>';
    }
  }

  return (list.innerHTML = data);
}

FetchAll();

//Add a new employee
my_btn2.addEventListener("click", () => {
  console.log("hello");

  document.getElementById("FirstName").value = "";
  document.getElementById("Age").value = "";
  document.getElementById("Salary").value = "";
});

//Reset button
my_btn1.addEventListener("click", () => {
  console.log("hello");

  controller
    .create({
      header: "Confirmation",
      message: "Are you sure you want to add this new employee?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            controller.dismiss(true);
            FirstName = document.getElementById("FirstName");
            Age = document.getElementById("Age");
            Salary = document.getElementById("Salary");
            // Get the value
            var name = FirstName.value;
            var age = Age.value;
            var Salaire = Salary.value;
            var myObj = {
              name: name,
              age: Age,
              salaire: Salaire
            };
            if (name) {
              countries.push(myObj);

              FirstName.value = "";
              Age.value = "";
              Salary.value = "";
              FetchAll();
          
            }
          }
        },
        {
          text: "No",
          handler: () => {
            controller.dismiss(true);
            return false;
          }
        }
      ]
    })
    .then(alert => {
      alert.present();
    });
});


//opin editing in a model
customElements.define(
  "modal-content",
  class ModalContent extends HTMLElement {
    connectedCallback() {
      const modalElement = document.querySelector('ion-modal');
      console.log(modalElement.componentProps.salaire);
      var data='   <ion-header translucent> \
      <ion-toolbar> \
          <ion-title>Modifier </ion-title> \
          <ion-buttons slot="end"> \
          <ion-button onclick="dismissModal()">Close</ion-button> \
        </ion-buttons> \
      </ion-toolbar></ion-header> \
      <ion-content fullscreen> \
      <ion-list lines="full" class="ion-no-margin ion-no-padding"> \
      <ion-item> \
          <ion-label position="floating">Name</ion-label> \
          <ion-input id="FirstNameE"  value='+modalElement.componentProps.name+' ></ion-input> \
      </ion-item> \
      <ion-item> \
          <ion-label position="floating">Age</ion-label> \
          <ion-input id="AgeE" value='+modalElement.componentProps.age+'></ion-input> \
      </ion-item> \
      <ion-item> \
          <ion-label position="floating">Salary</ion-label> \
          <ion-input id="SalaryE" value='+modalElement.componentProps.salaire+'></ion-input> \
      </ion-item> \
      </ion-list> \
      </ion-content fullscreen> \
      <ion-grid> \
          <div class="ion-padding"> \
             <ion-button  expand="block" type="submit"  onclick="Update('+modalElement.componentProps.item+')"class="ion-no-margin" color="primary">Create</ion-button> \
          </div> \
      </ion-grid> ' ;
      this.innerHTML = data ;
 

    ;
    }
  }
);

let currentModal = null;
const controllers = document.querySelector("ion-modal-controller");

//appel mpdel
function tUpdate(i) {
  controllers
    .create({
      component: "modal-content"
    })
    .then(modal => {
      modal.present();
      modal.componentProps = {
        'item':i,
        'name':countries[i].name,
        'age':countries[i].age,
        'salaire':countries[i].salaire,
      };
      currentModal = modal;
    });
}

//close model
function dismissModal() {
  if (currentModal) {
    currentModal.dismiss().then(() => { currentModal = null; });
  }
} 

// edit  employee
function Update(i) {
  FirstName = document.getElementById("FirstNameE");
  Age = document.getElementById("AgeE");
  Salary = document.getElementById("SalaryE");
  // Get the value
  var name = FirstName.value;
  var Age = Age.value;
  var Salaire = Salary.value;
  var myObj = {
    name: name,
    age: Age,
    salaire: Salaire
  };
  if (name) {
    countries.splice(i, 1, myObj);


    FirstName.value = "";
    Age.value = "";
    Salary.value = "";
    dismissModal();
    FetchAll();
  }

  console.log(countries);

}

// delete employee
function tDelete(i) {
  countries.splice(i, 1);   
  FetchAll();
}

